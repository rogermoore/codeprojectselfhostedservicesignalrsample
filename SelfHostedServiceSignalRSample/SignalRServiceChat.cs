﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Owin;
using System.ServiceProcess;

[assembly: OwinStartup(typeof(SelfHostedServiceSignalRSample.Startup))]
namespace SelfHostedServiceSignalRSample
{
    public partial class SignalRServiceChat : ServiceBase
    {
        public SignalRServiceChat()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if (!System.Diagnostics.EventLog.SourceExists("SignalRServiceChat"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "SignalRServiceChat", "Application");
            }
            eventLog1.Source = "SignalRServiceChat";
            eventLog1.Log = "Application";

            eventLog1.WriteEntry("In OnStart");            

            // This will *ONLY* bind to localhost, if you want to bind to all addresses
            // use http://*:8080 to bind to all addresses. 
            // See http://msdn.microsoft.com/en-us/library/system.net.httplistener.aspx 
            // for more information.
            string url = "http://localhost:8090";
            WebApp.Start(url);
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("In OnStop");
        }
    }
}
